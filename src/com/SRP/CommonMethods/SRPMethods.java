package com.SRP.CommonMethods;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class SRPMethods {

	public static void captureScreenShot(WebDriver driver, String value, String folder) throws IOException{
		File SRC=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(SRC, new File("C:\\workspace\\SRPPROJECT\\"+folder+"\\"+value+".png"));
		}
		
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	
	
}

