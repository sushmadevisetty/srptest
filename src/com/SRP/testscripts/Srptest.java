package com.SRP.testscripts;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.SRP.CommonMethods.SRPMethods;
import com.SRP.Excelutilities.Srpexcelutilites;
import com.SRP.Weblocators.Srplocators;



public class Srptest {
	WebDriver driver;
	@BeforeTest
	  public void beforeTest() throws IOException{  	
	
    	File dir1 = new File("TC1_Screenshots");  //Specify the Folder name here
		dir1.mkdir( );  
	System.setProperty("webdriver.chrome.driver", "C:\\sushma\\chromedriver_win32\\chromedriver.exe");	  
	driver = new ChromeDriver();
	      driver.manage().window().maximize();
	      System.out.println("Browser initialized");
	      Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 0, 0, 1, "Step1_Browser_Initialized");
	      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	      SRPMethods.captureScreenShot(driver, "Step1_Browser_initialization","TC1_Screenshots");
	      //ExcelUtilities.addScreen("C:/selenium/1_Browser_initialization.png", "C:\\Users\\Nethr\\workspace\\ProjectNewTours\\TestData\\Results.xlsx", 1, 0, 1);
	} 
	
	@Test(priority=1) //Login to specified URL
	  public void Login(){
		  try {	
	 //ExcelUtilities eu = new ExcelUtilities();
		 driver.get(Srpexcelutilites.getURL("C:\\workspace\\SRPPROJECT\\Testdata\\Testdata.xlsx", 1, 0, "URL"));
		// String baseUrl = eu.getURL("C:\\Users\\Nethr\\workspace\\ProjectNewTours\\TestData\\TestData.xlsx", 1, 1, "URL");
		System.out.println("Login page is displayed");
		SRPMethods.captureScreenShot(driver,"Step2_Login_Page_Displayed","TC1_Screenshots");
		Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 1, 0, 1, "Step2_Login_page_displayed");
		
		driver.findElement(By.xpath("//input[@name='UserName']")).sendKeys(Srpexcelutilites.getData("C:\\workspace\\SRPPROJECT\\Testdata\\Testdata.xlsx",1, 1, 1));
		driver.findElement(By.xpath(Srplocators.Password_xpath)).sendKeys(Srpexcelutilites.getData("C:\\workspace\\SRPPROJECT\\Testdata\\Testdata.xlsx",1, 1, 2));
		System.out.println("Password entered");
		SRPMethods.captureScreenShot(driver,"Step3_Username_Password_entered","TC1_Screenshots");
		driver.findElement(By.xpath(Srplocators.Login_xpath)).click();
		System.out.println("Sign in button clicked");	
		Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 2, 0, 1, "Step3_Signin_button_clicked");
		//ExcelUtilities.addScreen("C:/selenium/Signin_done.png", "C:\\Users\\Nethr\\workspace\\ProjectNewTours\\TestData\\Results.xlsx", 1, 0, 1);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}  catch(Exception e) {
	          e.printStackTrace();
	          System.out.println("Exception undergone");
	  }
		  
	  }
	
	@Test(priority=2) //accessing view bill link and selecting dropdown
	  public void viewbill() throws IOException {
		Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 3, 0, 1, "Step4_Homepage_displayed");
		driver.findElement(By.xpath(Srplocators.viewbill_xpath)).click();
		System.out.println("viewbill button clicked");
		Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 4, 0, 1, "Step5_view_button_clicked");
		SRPMethods.captureScreenShot(driver,"Step4_viewbill_Page_Displayed","TC1_Screenshots");
		Select Dropdown = new Select(driver.findElement(By.name(Srplocators.viewbillingfor_xpath)));
		//Dropdown.selectByVisibleText(ExcelUtilities.getData("C:\\workspace\\NewToursProject\\TestData\\TestData.xlsx",1,1,1));
		Dropdown.selectByVisibleText("May25,2019");  
		SRPMethods.captureScreenShot(driver,"Step5_viewbill_My25,2019_Displayed","TC1_Screenshots");
		Srpexcelutilites.writeresult("C:\\workspace\\SRPPROJECT\\Testdata\\Results.xlsx", 5, 0, 1, "Step6_viewbill_May25,2019_page dispalyed");
		
		
		
		
		
	}
	  
	  
	@AfterTest()  
	public void afterTest() throws IOException 
	{
		//NewToursMethods.captureScreenShot(driver, "Flight booked");
		driver.close();
	  
	}

}
