package com.SRP.Weblocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class srppom {
	
	private static WebElement element = null;
	
	public static WebElement  Sign_up_my_account(WebDriver driver) {
		element=  driver.findElement(By.xpath("//*[@id=\"newUserLink\"]/a"));
		return element;
	}
	
	public static  WebElement Email_address(WebDriver driver) {
	element=driver.findElement(By.xpath("//div[@class='srp-card-details']//form"));
	return element;
	}
	
	public static  WebElement Password(WebDriver driver) {
		element=driver.findElement(By.xpath("//input[@id='password']"));
		return element;
	}
	
	public static  WebElement Confirm_password(WebDriver driver) {
		element=driver.findElement(By.xpath("//input[@id='confirmPassword']"));
		return element;
	}
	
	public static  WebElement SRP_account_number(WebDriver driver) {
		element=driver.findElement(By.xpath("//input[@name='accountNumber']"));
		return element;
	}
	
	public static WebElement phone_Number(WebDriver driver){
		element=driver.findElement(By.xpath("//input[@id='phoneNumber']"));
		return element;
	}


	
	
			

}
