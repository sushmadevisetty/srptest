package com.SRP.Weblocators;

public class Srplocators {
	
	public static final String Sign_up_for_online_account="//a[contains(text(),'Sign up for online account access')]";
	
	public static final String User_name_xpath="//input[@name='Username']";
	
	public static final String Password_xpath="//input[@name='Password']";
	
	public static final String Login_xpath="//input[@id='loginSubmit']";
	
	public static final String viewbill_xpath="//a[text()='View bill']";
	
	//public static final String viewbillingfor_xpath="//div[contains(text(),'June 25, 2019')]";
	public static final String viewbillingfor_xpath="//div[@class='jss329 jss332 jss358 jss343']";
}
//Driver.findElement(By.xpath("//a[text()='App Configuration']")